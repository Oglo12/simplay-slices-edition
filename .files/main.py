from ursina import *

from ui_kit import *

window.borderless = False

app = Ursina()

main_menu = create_ui("ui_maps/main_menu.json")

app.run()
