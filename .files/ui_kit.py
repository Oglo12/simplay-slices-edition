from ursina import *
import json

UI_HARD_VALUES = {
        "button_texture": "white_cube",
        "button_model": "quad",
        "window_model": "quad",
        "window_texture": "white_cube",
        "window_color": color.rgb(0, 210, 255),
        "window_z": -0.001,
        }

def create_ui(ui_file):
    ui = None

    with open(ui_file, "r") as file:
        ui = json.load(file)

    ui_window = Entity(parent = camera.ui, model = UI_HARD_VALUES.get("window_model"), texture = UI_HARD_VALUES.get("window_texture"), color = UI_HARD_VALUES.get("window_color"), z = UI_HARD_VALUES.get("window_z"))

    ui_window.visible = True

    parts = {}

    for i in ui.get("parts"):
        type_id = i.get("type")
        config = i.get("config")
        name = i.get("name")

        buddy = None

        if type_id == "button":
            buddy = Button(model = UI_HARD_VALUES.get("button_model"), texture = UI_HARD_VALUES.get("button_texture"), text = config.get("text"))

        elif type_id == "entity":
            buddy = Entity(model = config.get("model"), texture = config.get("texture"))

        else:
            buddy = Entity() # Just to stop the program from crashing.

        buddy.parent = ui_window
        buddy.visible = True

        buddy.position = Vec3(config.get("x"), config.get("y"), 0)
        buddy.rotation = Vec3(0, 0, 0)
        buddy.color = color.rgb(config.get("r"), config.get("g"), config.get("b"))
        buddy.scale = Vec3(config.get("scale_x"), config.get("scale_y"), 0.001)

        parts[name] = buddy

    ui_window.parts = parts

    settings = ui.get("settings")

    winset = settings.get("window") # UI window settings.

    if winset.get("visible") == False:
        ui_window.visible = False
    else:
        ui_window.visible = True

    if winset.get("button_close") == False:
        ui_window.have_b_close = False
    else:
        ui_window.have_b_close = True

    return ui_window
